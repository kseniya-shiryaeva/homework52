import React from 'react';

class PokerHand {
    state = {
        cards: []
    }

    constructor(props) {
        this.state.cards = props.cards;
    }

    __checkFlushAndStraight = () => {
        let result = 'Flush';

        if (this.__checkStraight()) {
            result = 'Straight flush';
        }

        let royal = 0;
        const royalCards = ['T', 'K', 'Q', 'J', '10'];
        this.state.cards.forEach(item => {
            if(royalCards.indexOf( item ) !== -1 ) {
                royal++;
            }
        })
        if (royal === 5) {
            result = 'Royal flush';
        }

        return result;
    }

    __checkStraight = () => {
        let result = 'Straight';

        const keyArray = this.state.cards.map(item => {
            return item.key;
        })

        keyArray.sort((a, b) => a > b ? 1 : -1);

        let lastItem;
        keyArray.forEach(item => {
            if (lastItem && item !== (lastItem + 1)) {
                result = false;
            }
            lastItem = item;
        })

        return result;
    }

    __checkNumberOfSimilar = (similars, number) => {
        let result = false;

        for (const key in similars) {
            if (similars.hasOwnProperty(key) && similars[key] === number) {
                result = true;
            }
        }

        return result;
    }

    __checkSimilar = () => {
        let suits = {};
        let ranks = {};
        let result = '';
        this.state.cards.forEach(item => {
            this.state.cards.forEach(card => {
                if (card.key > item.key) {
                    if (card.suit === item.suit) {
                        if (suits.hasOwnProperty(item.suit)) {
                            suits[item.suit] = suits[item.suit] + 1;
                        } else {
                            suits[item.suit] = 2;
                        }
                    }
                    if (card.rank === item.rank) {
                        if (ranks.hasOwnProperty(item.rank)) {
                            ranks[item.rank] = ranks[item.rank] + 1;
                        } else {
                            ranks[item.rank] = 2;
                        }
                    }
                }
            })
        })


        if (Object.keys(ranks).length > 0) {
            if (this.__checkNumberOfSimilar(ranks, 6)) {
                result += 'Four of a kind';
            } else if (this.__checkNumberOfSimilar(ranks, 4)) {
                if (Object.keys(ranks).length === 2) {
                    result += 'Full house';
                } else {
                    result += 'Three of a kind';
                }
            } else if (this.__checkNumberOfSimilar(ranks, 2)) {
                if (Object.keys(ranks).length === 2) {
                    result += 'Two pairs';
                } else {
                    result += 'One pair';
                }
            }
        }

        if (Object.keys(suits).length > 0) {
            if (this.__checkNumberOfSimilar(suits, 10)) {
                result += this.__checkFlushAndStraight();
            } else {
                const straight = this.__checkStraight();
                result += straight ? straight : '' ;
            }
        }

        return result;
    }

    getOutcome() {
        if (this.state.cards.length > 0) {
            return this.__checkSimilar();
        } else {
            return '';
        }
    }
}

export default PokerHand;