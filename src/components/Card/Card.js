import React from 'react';
import './cards.css';
import './Card.css';

const suit = {
    'S': {
        'symbol': '♠',
        'name': 'spades'
    },
    'C': {
        'symbol': '♣',
        'name': 'clubs'
    },
    'H': {
        'symbol': '♥',
        'name': 'hearts'
    },
    'D': {
        'symbol': '♦',
        'name': 'diams'
    },
}

const Card = props => {
    const rank = props.rank.toLowerCase();
    return (
        <div className={`card rank-${rank} ${suit[props.suit].name}`}>
            <span className="rank">{props.rank}</span>
            <span className="suit">{suit[props.suit].symbol}</span>
        </div>
    );
}

export default Card;