import {Component} from "react";
import './App.css';
import CardDeck from "./CardDeck";
import PokerHand from "./PokerHand";
import Card from "./components/Card/Card";

class App extends Component {
    state = {
        cards: []
    }

    showCards = () => {
        const deck = new CardDeck();
        const cards = deck.getCards(5);
        this.setState({cards});
    }

    render() {
        const cards = this.state.cards.map(item => {
            return <Card suit={item.suit} rank={item.rank} />;
        })

        const hand = new PokerHand({cards: this.state.cards});
        const result = hand.getOutcome();
        return (
            <div className="App container">
                <div className="button-block">
                    <button onClick={this.showCards}>New cards</button>
                </div>
                <div className="result-block">
                    {result}
                </div>
                <div className="playingCards faceImages cards-container">
                    {cards}
                </div>
            </div>
        );
    }
}

export default App;