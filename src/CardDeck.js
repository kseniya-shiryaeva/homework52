import React from 'react';

class CardDeck {
    state = {
        deck: []
    }

    constructor() {
        const suits = ['S', 'C', 'H', 'D'];
        const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'T'];
        const deck = [];
        let key = 0;
        suits.forEach(suit => {
            ranks.forEach((rank) => {
                deck.push({key,suit,rank});
                key++;
            })
        })
        this.state.deck = deck;
    }

    getCard = () => {
        const randomIndex = Math.floor(Math.random() * (this.state.deck.length - 1));
        const [randomCard] = this.state.deck.splice(randomIndex, 1);
        return randomCard;
    }

    getCards = howMany => {
        const cardArray = [];

        for (let i = 0; i < howMany; i++) {
            cardArray.push(this.getCard());
        }

        return cardArray;
    }
}

export default CardDeck;